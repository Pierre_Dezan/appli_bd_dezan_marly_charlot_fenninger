<?php

use appli\Controler\ControlerMain;
use appli\controler\Logger;
use appli\controler\Seance3;
use appli\controler\Seance4;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;


require_once 'vendor/fzaninotto/faker/src/autoload.php';
require_once __DIR__."/vendor/autoload.php";
//require_once "src/models/ModelCharacter.php";
$cap = new \Illuminate\Database\Capsule\Manager();

$cap->addConnection(parse_ini_file("src/db.ini"));

$cap->setAsGlobal();
$cap->bootEloquent();

//$service = new Logger();

$return = "";

if(isset($_GET['question'])){
    switch($_GET['question']){
        case 1 :
            $a = ControlerMain::question1();
            break;
        case 2:
            $a = ControlerMain::question2();
            break;
        case 3:
            $a = ControlerMain::question3();
            break;
        case 4:
            $a = ControlerMain::question4();
            break;
        case 5:
            $a = ControlerMain::question5();
            break;
        case 6:
            $a = ControlerMain::question6();
            break;
        case 7:
            $a = ControlerMain::question7();
            break;
        case 8:
            $a = ControlerMain::question8();
            break;
        case 9:
            $a = ControlerMain::question9();
            break;
        case 10:
            $a = ControlerMain::question10();
            break;
        case 11:
            $a = ControlerMain::marioEtPlusTroisRating();
            break;
        case 12:
            $a = Seance3::question1();
            break;
        case 13:
            $a = Seance3::question2();
            break;
        case 14:
            $a = Seance3::question3();
            break;
        case 15:
            $a = Seance3::question4();
            break;
        case 16:
            $a = Seance3::question5();
            break;

        case 17:
            $a = seance4::EssaiFaker();
            break;


        case 18:
            $a = seance4::RemplirUtilisateur();
            break;

        case 19:
            $a = seance4::RemplirCom();
            break;
    }


    foreach ($a as $b){
        $return = $return . filter_var($b,FILTER_SANITIZE_STRING)."<br>";
    }
}

echo <<<END

<html>
<head>

</head>
<body>
<form action="" method="get" id="form">
<select name="question">
<option value="1">Question 1</option>
<option value="2">Question 2</option>
<option value="3">Question 3</option>
<option value="4">Question 4</option>
<option value="5">Question 5</option>
<option value="6">Question 6</option>
<option value="7">Question 7</option>
<option value="8">Question 8</option>
<option value="9">Question 9</option>
<option value="10">Question 10</option>
<option value="11">Question 11</option>
<option value="12">S3 P1 Q1</option>
<option value="13">S3 P1 Q2</option>
<option value="14">S3 P1 Q3</option>
<option value="15">S3 P1 Q4</option>
<option value="16">S3 P1 Q5</option>
<option value="17">Test faker</option>
<option value="18">Remplir utilisateurs</option>
<option value="19">Remplir Commentaires</option>

</select>
<button type="submit" form="form">Confirmer</button>
</form>
$return
</body>
</html>


END;
