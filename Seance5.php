<?php

use appli\Models\ModelCharacter;
use appli\Models\ModelCommentaires;
use appli\Models\ModelGame;
use appli\Models\ModelUtilisateur;

require_once 'vendor/autoload.php';

$cap = new \Illuminate\Database\Capsule\Manager();

$cap->addConnection(parse_ini_file("src/db.ini"));

$cap->setAsGlobal();
$cap->bootEloquent();



$app = new \Slim\Slim;
$app->get('/api/games/:id',function($id){
    $g = ModelGame::find($id);
    afficherJSON(json_encode(gameJson($g)));
    header('Content-Type: application/json');
});

$app->get('/api/games/',function(){
    $page = 0 ;
    if(isset($_GET['page'])){
        $page = $_GET['page'];
    }
    afficherJSON(json_encode(chargeJeux($page)));
    header('Content-Type: application/json');
});

$app->get('/api/comment/:id',function($id){
    $c = ModelCommentaires::find($id);
    afficherJSON(json_encode(array("id"=>$c->id,"titre"=>$c->titre,"contenu"=>$c->contenu,"date_creation"=>$c->created_at,"utilisateur"=>ModelUtilisateur::find($c->user_id)->nom)));
});

$app->get('/api/games/:id/comments',function($id){
    //Pour chaque commentaire retourné dans la collection, la représentation contient l'id, le titre, le texte,
    //la date de création et le nom de l'utilisateur.
    echo json_encode(array("comments"=>chargeComm($id)));

});

$app->post('/api/games/:id/comments',function($id){
    //ajout d'un comm
    $app = \Slim\Slim::getInstance();
    $json = $app->request->post('json');
    $txt = json_decode($json,true);
    $mail = $txt['email'];
    $titre = $txt['titre'];
    $contenu = $txt['contenu'];

    $comm = new ModelCommentaires();
    $comm->user_id = ModelUtilisateur::where('email','=',$mail)->first()->id;
    $comm->titre = $titre;
    $comm->contenu = $contenu;
    $comm->save();

    http_response_code(201);

    echo json_encode(array("href"=>array("self"=>"api/comment/".$comm->id)));
});

$app->get('/api/games/:id/platform',function($id){
    afficherJSON(json_encode(chargePlat($id)));
});

$app->get('/api/games/:id/characters',function($id){
    afficherJSON(json_encode(chargeChars($id)));
});

$app->get('/api/character/:id',function($id){
    $c = ModelCharacter::find($id);
    afficherJSON(json_encode(array("character"=>array("id"=>$c->id,"name"=>$c->name,"links"=>array("self"=>"/api/character/".$c->id)))));
});

$app->get('/api/platform/:id',function($id){
    $c = \appli\Models\ModelPlatform::find($id);
    afficherJSON(json_encode(array("id"=>$c->id,"name"=>$c->name,"alias"=>$c->alias,"abbreviation"=>$c->abbreviation,"links"=>array("self"=>"/api/platform/".$c->id))));
});

$app->run();

function chargePlat($id){
    $g = ModelGame::find($id);
    $comp = $g->platform()->get();
    //afficher($char);
    $return = array();
    foreach ($comp as $c){
        $txt = array("id"=>$c->id,"name"=>$c->name,"alias"=>$c->alias,"abbreviation"=>$c->abbreviation,"links"=>array("self"=>"/api/platform/".$c->id));
        $return[] = $txt;
    }
    return $return;
}

function chargeChars($id){
    $g = ModelGame::find($id);
    $char = $g->char()->get();
    //afficher($char);
    $return = array();
    foreach ($char as $c){
        $txt = array("character"=>array("id"=>$c->id,"name"=>$c->name,"links"=>array("self"=>"/api/character/".$c->id)));
        $return[] = $txt;
    }
    return $return;
}


function chargeComm($id){
    $comm = ModelCommentaires::where('game_id','=',$id)->get();
    $txt = array();
    foreach ($comm as $c){
        $name = $c->user_id;
        $name = ModelUtilisateur::find($name)->nom;
        //$name = "yolo";
        $txt[] = array("id"=>$c->id,"titre"=>$c->titre,"contenu"=>$c->contenu,"date_creation"=>$c->created_at,"utilisateur"=>$name);
    }
    return $txt;
}

function chargeJeux($page){
    $games =  ModelGame::where([['id','>=',$page*200],['id','<',($page+1)*200]])->get();
    $gamejson = array();
    foreach ($games as $g){
        $gamejson[] = gameJson($g);
    }
    $prev = $page-1;
    $next = $page+1;
    if($prev < 0) $prev = 0;
    $links = array('prev'=>array('href'=>'/api/games?page='.$prev),'next'=>array('href'=>'/api/games?page='.$next));
    return(array('games'=>$gamejson,'links'=>$links));
}

function gameJson($g){
    $id = filter_var($g->id,FILTER_SANITIZE_STRING);
    $name = filter_var($g->name,FILTER_SANITIZE_STRING);
    $alias = filter_var($g->alias,FILTER_SANITIZE_STRING);
    $deck = filter_var($g->deck,FILTER_SANITIZE_STRING);
    $desc = filter_var($g->description,FILTER_SANITIZE_STRING);
    $rel = filter_var($g->original_release_date,FILTER_SANITIZE_STRING);
    $links = array("comments"=>"/api/games/".$g->id."/comments","characters"=>"/api/games/".$g->id."/characters","platforms"=>chargePlat($g->id));
    $txt = array("id"=>$id,"name"=>$name,"alias"=>$alias,"deck"=>$deck,"description"=>$desc,"original_release_date"=>$rel,"links"=>array("self"=>$links));
    //var_dump($txt);
    return $txt;
}

function afficherJSON($content){
    echo $content;
}

function afficher($content){
    $content = filter_var(json_encode($content),FILTER_SANITIZE_STRING);
    echo <<<END
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
    $content
</body>
</html>
END;
}

