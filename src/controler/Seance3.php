<?php
/**
 * Created by PhpStorm.
 * User: redma
 * Date: 03/04/2018
 * Time: 14:10
 */

namespace appli\controler;


use appli\Models\ModelGame;

class Seance3
{
    //PARTIE 1
   public static function question1(){
       $time = microtime(true);
        ModelGame::all();
        return array(microtime(true)-$time."s");
   }

    public static function question2(){
       $time = microtime(true);
        ModelGame::where('name','like','Mario%')->get();
        return array(microtime(true)-$time . "s");
    }

    public static function question3(){
        $time = microtime(true);
        $tmp = ModelGame::where('name','like','%Mario%')->get();
        $arr = array();
        foreach($tmp as $i){
            $arr[] = $i;
        }
        return array(microtime(true)-$time . "s");
    }

    public static function question4()
    {
        $time = microtime(true);
        $tmp = ModelGame::where('name','like','%Mario%')->get();
        foreach($tmp as $i){
            if(count($i->char()->get() ) >3){
                $res[] = $i->id;
            }
        }
        return array(microtime(true)-$time . "s");
    }

    //seconde partie de la première partie( ça a aucun sens bordel de merde)
    public static function question5(){
        $names = array("%Mario%","%Zelda%","%Sonic%");
        $ret = array();
        foreach ($names as $n) {
            //echo($n);
            $time = microtime(true);
            $tmp = ModelGame::where('name', 'like', $n)->get();
            $arr = array();
            foreach ($tmp as $i) {
                $arr[] = $i;
            }
            $ret = array_merge($ret,array($n ." : ". (microtime(true)-$time) . "s , " . count($arr). " entrées"));
        }
        return $ret;
    }
}