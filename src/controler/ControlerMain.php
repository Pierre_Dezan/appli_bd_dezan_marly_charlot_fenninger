<?php

namespace appli\Controler;
use appli\Models\ModelCompany;
use appli\models\ModelGame;
use appli\models\ModelPlatform;

/**
 * Created by PhpStorm.
 * User: redma
 * Date: 14/03/2018
 * Time: 15:24
 */


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
class ControlerMain
{
    //seance 1
    public static function question1(){
        return ModelGame::where('name','like','%Mario%')->get();
    }

    public static function question2(){
        return ModelCompany::where('location_country','like','%Japan%')->get();
    }

    public static function question3(){
        return ModelPlatform::where('install_base','>=','10000000')->get();
    }

    public static function question4(){
        return ModelGame::where('id','>=','21173')->take(442)->get();
    }

    public static function question5(){

        if($_GET['page']==NULL){
            $_POST['page']=0;
        }
      $count = ModelGame::get()->count();
      if($count/500 != 0)
        $nbp=($count/500)+1;
      else $nbp=$count/500;
      if($_GET['page']<=$nbp) {
          $pgcour = $_GET['page'];
      }else{
          $pgcour = $nbp;
          $_POST['page']= $nbp;
      }
      $tmp = ModelGame::where('id','>=',$pgcour*500)->take($pgcour*500+500)->get();


        foreach ($tmp as $g){
            $return[] = $g->name . '  '.$g->deck.'<br>';
        }
        return $return;
     //return view('game.id',['nom'=>$tmp->name,'deck'=>$tmp->deck]);


    }

    //seance 2
    public static function question6(){
        $ga = ModelGame::find(12342)->char()->get();

        return $ga;
    }

    public static function question7(){
        $tmp = ModelGame::where('name','like','Mario%')->get();
        $return = array();
        foreach ($tmp as $g){
            //array_merge($return,$g->char()->get());
            //$return[] = $g->char()->get();
            foreach ($g->char as $ch){
                $return[] = $ch->name . '<br>';
            }
        }
        return $return;
    }

    public static function question8(){
        $tmp = ModelCompany::where('name','like','%Sony%')->get();
        $return = array();
        foreach ($tmp as $c){
            foreach ($c->games as $g) {
                $return[] = $g->name . '<br>';
            }
        }
        return $tmp;
    }


    public static function question9(){
      $tmp = ModelGame::where('name','like','%Mario%')->get();
      $res =array();
      foreach($tmp as $i){
        $res[] = $i->rating()->get();
      }
      return $res;
    }

    public static function question10(){
      $tmp = ModelGame::where('name','like','Mario%')->get();
      $tab = array();
      foreach($tmp as $i){
        if(count($i->char()->get() ) >3){
          $res[] = $i->id;
        }
      }
      return $res;
    }

    public static function marioEtPlusTroisRating(){
        $tmp = ModelGame::where('name','like','Mario%')->get();
        $tab = array();
        foreach($tmp as $i){
            $tmp2 = $i->rating()->where('rating_board.name','like','%3+%')->get();
            $tab[] = $tmp2->id;

        }
        return $tab;
    }
    public static function question11(){

    }


}
