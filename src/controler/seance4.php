<?php

namespace appli\controler;

use appli\Models\ModelUtilisateur;
use vendor\fzaninotto\faker;

use appli\Models\ModelGame;

use appli\Models\ModelCommentaires;


class seance4
{

    public static function EssaiFaker(){

$faker = \Faker\Factory::create();


echo $faker->name;

echo $faker->address;

echo $faker->text;
    }


    public static function FakerUtilisateur()
    {
        $faker = \Faker\Factory::create();
        $nom = $faker->lastName;
        $prenom = $faker->firstName;
        $email = $faker->email;
        $adresse = $faker->address;
        $tel = $faker->phoneNumber;
        $datenaiss = $faker->date($format = 'Y-m-d', $max = 'now');
        $u = new ModelUtilisateur();
        $u->nom=$nom;
        $u->prenom=$prenom;
        $u->email=$email;
        $u->adresse=$adresse;
        $u->telephone=$tel;
        $u->date_de_naissance=$datenaiss;
        $u->save();
    }


    public static function RemplirUtilisateur()
    {
        $tab = array();
        
        ini_set(max_execution_time, 300);

        $faker = \Faker\Factory::create('fr_FR');

        for ($i = 0; $i < 25000; $i++) {

            $email = $faker->email;
            while (isset($tab[$email])){
                $email = $faker->email;
            }
            $tab[$email] = 1;

            $nom = $faker->lastName;


            $prenom = $faker->firstName;

            $adresse = $faker->address;

            $telephone = $faker->phoneNumber;

            $naiss = $faker->dateTimeThisCentury->format('Y-m-d');

            $utilisateur = new ModelUtilisateur;

            $utilisateur->email = $email;

            $utilisateur->nom = $nom;

            $utilisateur->prenom = $prenom;

            $utilisateur->adresse = $adresse;

            $utilisateur->telephone = $telephone;

            $utilisateur->date_de_naissance = $naiss;

            $utilisateur->save();
        }
    }

    public static function RemplirCom(){

        ini_set('memory_limit','512M');
        ini_set(max_execution_time, 300);
        $c = ModelGame::all()->count();

        $tmp2 = ModelUtilisateur::all('email')->toArray();

       // $tmp2 = $tmp->toArray();
        $c2 = sizeof($tmp2);

        $faker = \Faker\Factory::create('fr_FR');

        for ($i = 0; $i < 250000; $i++) {

        $id_jeu = rand(1,$c);

        $r = rand(1,$c2);


        $email = $tmp2[$r]['email'];


        $contenu = $faker->text($maxNbChars = 200);

        $titre = substr($contenu, 0, 10);

        $creation = $faker->dateTimeBetween($startDate = '-2 years', $endDate = 'now', $timezone = null);

        $update = $faker->dateTimeBetween($creation, $endDate = 'now', $timezone = null);

        $commentaire = new ModelCommentaires;

        $commentaire->game_id = $id_jeu;

        $commentaire->titre = $titre;

        $commentaire->contenu = $contenu;

        $commentaire->created_at = $creation;

        $commentaire->updated_at = $update;

        $commentaire->email = $email;

        $commentaire->save();
    
        }
    }

}