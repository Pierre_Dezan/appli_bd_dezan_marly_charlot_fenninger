<?php
namespace appli\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Created by PhpStorm.
 * User: redma
 * Date: 14/03/2018
 * Time: 14:41
 */

class ModelCharacter extends Model
{
    protected $primaryKey = "id";
    protected $table = "character";
    public $timestamps = false;

}