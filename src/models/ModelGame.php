<?php
namespace appli\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Created by PhpStorm.
 * User: redma
 * Date: 14/03/2018
 * Time: 14:41
 */

class ModelGame extends Model
{
    protected $primaryKey = "id";
    protected $table = "game";
    public $timestamps = false;

    public function char(){
        return $this->belongsToMany('appli\Models\ModelCharacter','game2character','game_id','character_id');
    }

    public function company(){
        return $this->belongsToMany('appli\Models\ModelCompany','game_publishers','game_id','comp_id');
    }

    public function rating(){
      return $this->belongsToMany('appli\Models\ModelRating','game_rating','id','rating_board_id');
    }

    public function platform(){
        return $this->belongsToMany('appli\Models\ModelPlatform','game2platform','game_id','platform_id');
    }

    public function comm(){
        return $this->hasMany('appli\Models\ModelCommentaires');
    }


}
