<?php
namespace appli\Models;
use Illuminate\Database\Eloquent\Model;

class ModelRating extends Model
{
    protected $primaryKey = "id";
    protected $table = "rating_board";
    public $timestamps = false;

}
