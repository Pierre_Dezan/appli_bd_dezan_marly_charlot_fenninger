<?php
namespace appli\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Created by PhpStorm.
 * User: redma
 * Date: 14/03/2018
 * Time: 14:41
 */

class ModelCompany extends Model
{
    protected $primaryKey = "id";
    protected $table = "company";
    public $timestamps = false;


    public function games(){
        return $this->belongsToMany('appli\Models\ModelGame','game_publishers','comp_id','game_id');
    }

}