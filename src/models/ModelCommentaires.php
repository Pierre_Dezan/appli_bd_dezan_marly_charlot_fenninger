<?php
namespace appli\Models;
use Illuminate\Database\Eloquent\Model;

class ModelCommentaires extends Model
{
    protected $primaryKey = "id";
    protected $table = "commentaires";
    public $timestamps = true;

    public function utilisateur(){
        return $this->belongsToMany('appli\Models\ModelUtilisateur','commentaires','id','titre', 'contenu','created_at','updated_at','email');
    }

}