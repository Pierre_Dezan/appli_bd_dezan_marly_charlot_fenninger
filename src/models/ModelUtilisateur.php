<?php
namespace appli\Models;
use Illuminate\Database\Eloquent\Model;

class ModelUtilisateur extends Model
{
    protected $primaryKey = "id";
    protected $table = "utilisateur";
    public $timestamps = false;

    public function commentaires(){
        return $this->belongsToMany('appli\Models\ModelCommentaires','commentairesutilisateur','email','id');
    }

}

